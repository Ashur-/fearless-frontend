import React, { useEffect, useState } from 'react';


function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [presenter_Name, setPresenter_Name] = useState('');
  const [presenter_Email, setPresenter_Email] = useState('');
  const [company_Name, setCompany_Name] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [Conference, setConference] = useState('');

  const handlePresenter_NameChange = (event) => {
    const value = event.target.value;
    setPresenter_Name(value);
  };

  const handlePresenter_EmailChange = (event) => {
    const value = event.target.value;
    setPresenter_Email(value);
  };

  const handleCompany_NameChange = (event) => {
    const value = event.target.value;
    setCompany_Name(value);
  };

  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  };

  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  };

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      presenter_name: presenter_Name,
      presenter_email: presenter_Email,
      company_name: company_Name,
      title,
      synopsis,
    };

    const conferenceId = Conference;
    const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);

      setPresenter_Name('');
      setPresenter_Email('');
      setCompany_Name('');
      setTitle('');
      setSynopsis('');
      setConference('');
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const selectTag = document.getElementById('conference');

    const handleDOMContentLoaded = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        for (let conference of data.conferences) {
          const option = document.createElement('option');
          option.value = conference.id;
          option.innerHTML = conference.name;
          selectTag.appendChild(option);
        }
      }
    };

    document.addEventListener('DOMContentLoaded', handleDOMContentLoaded);

    return () => {
      document.removeEventListener('DOMContentLoaded', handleDOMContentLoaded);
    };
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresenter_NameChange} value={presenter_Name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresenter_EmailChange} value={presenter_Email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompany_NameChange} value={company_Name} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} value={Conference} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => (
                    <option key={conference.id} value={conference.id}>
                      {conference.name}
                    </option>
                  ))}
                </select>
              </div>
              <button className="btn btn-primary" type="submit">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;