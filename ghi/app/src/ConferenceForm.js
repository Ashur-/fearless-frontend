import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [max_presentations, setMax_Presentations] = useState('');
  const [max_attendees, setMax_Attendees] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  };

  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  };

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  };

  const handleMax_PresentationsChange = (event) => {
    const value = event.target.value;
    setMax_Presentations(value);
  };

  const handleMax_AttendeesChange = (event) => {
    const value = event.target.value;
    setMax_Attendees(value);
  };
  
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name,
      starts,
      ends,
      description,
      max_presentations,
      max_attendees,
      location,
    };

    console.log(data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
    //   console.log(newConference);

      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMax_Presentations('');
      setMax_Attendees('');
    }
  };

  const fetchLocations = async () => {
    const response = await fetch('http://localhost:8000/api/locations/');
    if (response.ok) {
      const data = await response.json();
      console.log(data.locations);
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchLocations();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-md-3 col-md-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} type="date" id="start_date" name="starts" required className="form-control"/>
                <label htmlFor="start_date" className="form-label">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} type="date" id="end-date" name="ends"required className="form-control"/>
                <label htmlFor="end-date" className="form-label">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} name="description" id="description" required className="form-control"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMax_PresentationsChange} value={max_presentations} type="number" id="max-presentations" name="max_presentations" required className="form-control" placeholder="Maximum Presentations"/>
                <label htmlFor="max-presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMax_AttendeesChange} value={max_attendees} type="number" id="max-attendees" name="max_attendees" required className="form-control" placeholder="Maximum Attendees"/>
                <label htmlFor="max-attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map((location) => (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  ))}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
