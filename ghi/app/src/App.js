import { BrowserRouter as Router, Routes, Route, } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import SignupForm from './SignupForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <Router>
      <>
        <Nav />
        <div className="container">
          <Routes>
          <Route path="/mainpage" element={<MainPage />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<SignupForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          </Routes>
        </div>
      </>
    </Router>
  );
}

export default App;
