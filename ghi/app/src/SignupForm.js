import React, { useState, useEffect } from 'react';

function SignUpForm() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [conference, setConference] = useState('');
  const [conferences, setConferences] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setLoading(true);

    const data = {
      name,
      email,
      conference,
    };

    try {
      const response = await fetch('http://localhost:8001/api/attendees/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });

      if (response.ok) {
        // Attendee sign-up successful
        setName('');
        setEmail('');
        setConference('');
        setLoading(false);
        alert('Attendee sign-up successful!');
      } else {
        // Attendee sign-up failed bruv
        setLoading(false);
        alert('Failed to sign up attendee. Please try again.');
      }
    } catch (error) {
      // Attendee sign-up failed due to network error
      setLoading(false);
      alert('Failed to sign up attendee due to a network error. Please try again.');
    }
  };

  useEffect(() => {
    (async () => {
      const url = 'http://localhost:8000/api/conferences/';
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        } else {
          alert('Failed to fetch conferences. Taking Ls i see.');
        }
      } catch (error) {
        alert('Failed to fetch conferences. GitGud');
      }
    })();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-md-3 col-md-6">
          <div className="shadow p-4 mt-4">
            <h1>Attendee Sign Up</h1>
            <form onSubmit={handleSubmit} id="create-attendee-form">
              <div className="form-floating mb-3">
                <input type="text" name="name" value={name} onChange={handleNameChange} placeholder="Name" required className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input type="email" name="email" value={email} onChange={handleEmailChange} placeholder="Email" required className="form-control"/>
                <label htmlFor="email">Email</label>
              </div>
              <div className="mb-3">
                <select name="conference" value={conference} onChange={handleConferenceChange} required className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => (
                    <option key={conference.href} value={conference.href}>
                      {conference.name}
                    </option>
                  ))}
                </select>
              </div>
              <button className="btn btn-primary">Sign Up</button>
            </form>
            {loading && <p>Loading...</p>}
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignUpForm;
