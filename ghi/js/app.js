function createCard(name, description, pictureUrl, startDate, endDate, subtitle) {
    const formatDateString = (dateString) => {
        if (!dateString) return 'N/A'; // Fallback if date is missing
    
        const date = new Date(dateString);
        if (isNaN(date)) return 'Invalid Date'; // Fallback if date is invalid
    
        return date.toLocaleDateString('en-US');
      };
    
      const formattedStartDate = formatDateString(startDate);
      const formattedEndDate = formatDateString(endDate);

    return `
      <div class="col-md-4">
        <div class="card shadow border-light mb-5 bg-body-tertiary rounded">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle text-muted mb-2">${subtitle}</p>
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
          <p class="card-date">${formattedStartDate} - ${formattedEndDate}</p>
          </div>
        </div>
      </div>
    `;
  }
  
  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        throw new Error('Unable to fetch conference details.');
        // Handle the case when the response is not OK
      } else {
        const data = await response.json();
        const conferences = data.conferences;
  
        for (let conference of conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
  
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            const subtitle = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, endDate, subtitle);
            console.log(html);
  
            // Add the generated HTML to the innerHTML of the columns
            const rowContainer = document.querySelector('.row');
            rowContainer.innerHTML += html;
          }
        }
    }
    } catch (error) {
      console.error(error);
      console.log("you got an error bruv")
    }
});